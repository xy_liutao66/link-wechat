package com.linkwechat.wecom.domain.dto.moments;

import com.linkwechat.wecom.domain.dto.WeResultDto;
import lombok.Data;

@Data
public class MomentsResultDto extends WeResultDto {

     private String jobid;
}
